import { DemoChartJsPage } from './app.po';

describe('demo-chart-js App', function() {
  let page: DemoChartJsPage;

  beforeEach(() => {
    page = new DemoChartJsPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
