import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Chart } from 'chart.js';
import { AppComponent } from './app.component';

import { AnnualTimespentChart } from './annual-timespent/annual-timespent';
import { ChildAnnualTimespentChart } from './annual-timespent/child-annual-timespent/child-annual-timespent';
import { QuarterlyTimespentChart } from './quarterly-timespent/quarterly-timespent';
import { ChildQuarterlyTimespentChart } from './quarterly-timespent/child-quarterly-timespent/child-quarterly-timespent';
@NgModule({
  declarations: [
    AppComponent,
    AnnualTimespentChart,
    QuarterlyTimespentChart,
    ChildAnnualTimespentChart,
    ChildQuarterlyTimespentChart
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
