import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  datasetsAnnualTimeSpent: any = {
    Q1: { gov: 10, enter: 10, smb: 30 },
    Q2: { gov: 20, enter: 20, smb: 20 },
    Q3: { gov: 20, enter: 20, smb: 20 },
    Q4: { gov: 20, enter: 20, smb: 0 },
    line: 10
  };
  datasetQuarterlyTimeSpent: any = {
    Q1: { gov: 10, enter: 10, smb: 30 },
  }
  constructor() {
  }
  ngOnInit() {

  }
}

