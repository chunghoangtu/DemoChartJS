import { Component, OnInit, Input } from '@angular/core';
import Chart from 'chart.js';

@Component({
    selector: 'quarterly-timespent-chart',
    templateUrl: './quarterly-timespent.html'
})

export class QuarterlyTimespentChart implements OnInit {
    @Input('datasets')
    datasets: any;
    constructor() { }
    ngOnInit() {  }
}