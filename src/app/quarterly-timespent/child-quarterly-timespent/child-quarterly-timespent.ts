import { Component, OnInit, Input } from '@angular/core';
import Chart from 'chart.js';

@Component({
    selector: 'child-quarterly-timespent',
    templateUrl: './child-quarterly-timespent.html'
})

export class ChildQuarterlyTimespentChart implements OnInit {
    @Input('datasets')
    datasets: any;
    constructor() { }
    ngOnInit() { 
        console.log(this.datasets);
     }
}