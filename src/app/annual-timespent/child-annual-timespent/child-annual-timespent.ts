import { Component, OnInit, Input } from '@angular/core';
import Chart from 'chart.js';

@Component({
    selector: 'child-annual-timespent',
    templateUrl: './child-annual-timespent.html'
})

export class ChildAnnualTimespentChart implements OnInit {
    @Input('datasets')
    datasets: any;    
    constructor() { }
    ngOnInit() {
        console.log(this.datasets);
    }
}